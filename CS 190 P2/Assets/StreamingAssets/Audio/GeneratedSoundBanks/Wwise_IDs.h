/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ALARNOFF = 2938998662U;
        static const AkUniqueID ALARNON = 927833256U;
        static const AkUniqueID CHICKEN = 4251222990U;
        static const AkUniqueID DOGHAPPY = 251210461U;
        static const AkUniqueID DOGIDLE = 3689162855U;
        static const AkUniqueID DOGIDLEOFF = 1258089390U;
        static const AkUniqueID DOGWALKING = 1264111578U;
        static const AkUniqueID DOORCLOSE = 3638457343U;
        static const AkUniqueID DOOROPEN = 1404805401U;
        static const AkUniqueID DOORWOODOPEN = 167128210U;
        static const AkUniqueID GENERATORDOWN = 2882088552U;
        static const AkUniqueID GHOSTMOVING = 1998017376U;
        static const AkUniqueID GHOSTVANISH = 2626477823U;
        static const AkUniqueID HORROR = 1040437717U;
        static const AkUniqueID NIGHTTIME = 1738206610U;
        static const AkUniqueID OBJECTIVEDONE = 3681650294U;
        static const AkUniqueID PANIC = 3130232582U;
        static const AkUniqueID PEEING = 4268514363U;
        static const AkUniqueID PICKUPFLASHLIGHT = 612434275U;
        static const AkUniqueID PLAYERDYING = 2554669961U;
        static const AkUniqueID PLAYERHURT = 3537581393U;
        static const AkUniqueID PLAYERWALK = 1592629277U;
        static const AkUniqueID PLAYERWALKBED = 1884340074U;
        static const AkUniqueID PLAYERWALKCONCRETE = 4292516U;
        static const AkUniqueID PLAYERWALKWATER = 1869761570U;
        static const AkUniqueID SHOWERACTIVE = 2774017909U;
        static const AkUniqueID SNOREACTIVE = 2122503292U;
        static const AkUniqueID SNOREOFF = 4036702613U;
        static const AkUniqueID SWITCHLIGHTON = 682380620U;
        static const AkUniqueID ZOMBIEWALKING = 2727484424U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace BGM_FILTER
        {
            static const AkUniqueID GROUP = 863150862U;

            namespace STATE
            {
                static const AkUniqueID LPF_01 = 2680780729U;
                static const AkUniqueID LPF_02 = 2680780730U;
                static const AkUniqueID LPF_03 = 2680780731U;
                static const AkUniqueID LPF_04 = 2680780732U;
                static const AkUniqueID LPF_05 = 2680780733U;
                static const AkUniqueID LPF_06 = 2680780734U;
                static const AkUniqueID LPF_07 = 2680780735U;
                static const AkUniqueID LPF_08 = 2680780720U;
                static const AkUniqueID LPF_09 = 2680780721U;
                static const AkUniqueID LPF_10 = 2697558315U;
                static const AkUniqueID LPF_11 = 2697558314U;
                static const AkUniqueID LPF_12 = 2697558313U;
            } // namespace STATE
        } // namespace BGM_FILTER

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
