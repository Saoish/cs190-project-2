/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BGM = 412724365U;
        static const AkUniqueID PICKUP_SPRITE = 70917955U;
        static const AkUniqueID PLAY_YOU_WIN = 2934514636U;
        static const AkUniqueID SPRITE_EMITTER = 3875712441U;
        static const AkUniqueID SPRITE_STOPPER = 683008598U;
        static const AkUniqueID WALL_BOTTOM = 49195805U;
        static const AkUniqueID WALL_LEFT = 67109807U;
        static const AkUniqueID WALL_RIGHT = 1527804914U;
        static const AkUniqueID WALL_TOP = 666314901U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace BGM_FILTER
        {
            static const AkUniqueID GROUP = 863150862U;

            namespace STATE
            {
                static const AkUniqueID LPF_01 = 2680780729U;
                static const AkUniqueID LPF_02 = 2680780730U;
                static const AkUniqueID LPF_03 = 2680780731U;
                static const AkUniqueID LPF_04 = 2680780732U;
                static const AkUniqueID LPF_05 = 2680780733U;
                static const AkUniqueID LPF_06 = 2680780734U;
                static const AkUniqueID LPF_07 = 2680780735U;
                static const AkUniqueID LPF_08 = 2680780720U;
                static const AkUniqueID LPF_09 = 2680780721U;
                static const AkUniqueID LPF_10 = 2697558315U;
                static const AkUniqueID LPF_11 = 2697558314U;
                static const AkUniqueID LPF_12 = 2697558313U;
            } // namespace STATE
        } // namespace BGM_FILTER

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
