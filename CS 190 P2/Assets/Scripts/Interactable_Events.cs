﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable_Events : MonoBehaviour {

    public int priority = 0;
    public string objective_description;
    
    public bool interactable = false;

    public Vector2 Position {
        get { return transform.position; }
    }
   
    protected virtual void Awake() {
        gameObject.tag = "interaction";
    }

    // Use this for initialization
    protected virtual void Start () {
		
	}

    // Update is called once per frame
    protected virtual void Update () {
		
	}



    public abstract void interact();

    public abstract void indicate();
}
