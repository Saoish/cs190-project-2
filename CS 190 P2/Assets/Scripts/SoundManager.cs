﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    
    public static SoundManager instance;

    public string BankName = "Main";

    //Events Name:
    public static string PlayerWalk = "PlayerWalk";
    public static string PlayerWalkCarpet = "PlayerWalkCarpet";
    public static string PlayerWalkWater = "PlayerWalkWater";
    public static string PlayerWalkWood = "PlayerWalkWood";

    public static string DoorOpen = "DoorOpen";
    public static string DoorClose = "DoorClose";
    public static string Peeing = "Peeing";
    public static string ShowerActive = "ShowerActive";

    public static string DogIdle = "DogIdle";
    public static string DogHappy = "DogHappy";
    //public static string DogIdleOff = "DogIdleOff";
    public static string DogDie = "DogDie";


    public static string AlarmOn = "AlarnOn";
    public static string AlarmOff = "AlarnOff";

    public static string SnoreActive = "SnoreActive";
    public static string SnoreOff = "SnoreOff";

    public static string NightTime = "NightTime";

    public static string pickupFlashlight = "pickupFlashlight";

    public static string SwitchLightOn = "SwitchLightOn";
    public static string SwitchLightOff = "SwitchLightOff";

    public static string GeneratorDown = "GeneratorDown";
    public static string GhostMoving = "GhostMoving"; 
    public static string GhostVanish = "GhostVanish"; 

    public static string PlayerHurt = "PlayerHurt"; 
    public static string PlayerDying = "PlayerDying";

    public static string Panic = "Panic";
    public static string RapidBreath = "RapidBreath";

    public static string ObjectiveDone = "ObjectiveDone";

    public static string Horror = "Horror";

    public static string EndGame = "EndGame";

    public static string ButtonHover = "ButtonHover";
    public static string ButtonClick = "ButtonClick";

    public static string Swoosh = "Swoosh";
         
         

    private void Awake() {
        if(instance!=null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            //AkBankManager.UnloadBank("Main");
            //AkBankManager.LoadBankAsync("Main");
            instance = this;
            DontDestroyOnLoad(this);
        }        
    }

    private void Start() {
        
    }

    public static void Post(string Event, GameObject go)
    {

        AkSoundEngine.PostEvent(Event, go);
    }

    public static void Post(string Event, GameObject go, AkCallbackManager.EventCallback myfunction)
    {

        AkSoundEngine.PostEvent(Event, go, (uint)AkCallbackType.AK_EndOfEvent, myfunction, null);
    }

    public static void playHorrorSong()
    {
        Post(Horror, instance.gameObject);
    }

}
