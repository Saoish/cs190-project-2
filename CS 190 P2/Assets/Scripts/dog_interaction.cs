﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dog_interaction : Interactable_Events {

    public float Delay = 0f;

    public GameObject blood;

    IEnumerator StartDelay()
    {
        yield return new WaitForSeconds(Delay);
        //SoundManager.Post();
        SoundManager.Post(SoundManager.DogIdle, gameObject);
    }

    protected override void Start()
    {
        base.Start();
        StartCoroutine(StartDelay());
    }

    public override void indicate() {
        Hint.show("'E' pet the dog");
    }

    public override void interact() {
        if (interactable) {
            //Stop SFX
            SoundManager.Post(SoundManager.DogHappy, gameObject);
            PlayerController.IE = null;
            interactable = false;
            ObjectiveManager.Next();
            Hint.reset();
        }
    }

    public void die()
    {
        blood.SetActive(true);
        SoundManager.Post(SoundManager.DogDie, gameObject);
        Destroy(transform.parent.gameObject);
    }
}
