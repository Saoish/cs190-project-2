﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toilet_interaction : Interactable_Events
{
    private void Start()
    {        

    }

    public override void indicate() {
        Hint.show("'E' pee");
    }

    public override void interact()
    {

        if (interactable)
        {
            Debug.Log("Freeze! while peeing!");            
            PlayerController.allowControl = false;
            SoundManager.Post(SoundManager.Peeing, gameObject, afterPee);



            //for testing:
            //AkSoundEngine.PostEvent(SoundManager.PlayerWalk, gameObject, (uint)AkCallbackType.AK_EndOfEvent, afterPee, "pass me!");

            PlayerController.IE = null;
            interactable = false;
            Hint.reset();
            //stun the player while peeing:
            //older way:
            //StartCoroutine(pausePlayerController(pee_duration));      //pee duration is a float
        }        

    }

    public void afterPee(object in_cookie, AkCallbackType in_type, object in_info)
    {

        if (in_type == AkCallbackType.AK_EndOfEvent)
        {
            Debug.Log("pee stop! can move now!");
            Hint.reset();
            PlayerController.allowControl = true;
            interactable = false;
            ObjectiveManager.Next();
            //AkEventCallbackInfo info = (AkEventCallbackInfo)in_info; //Then do stuff.

        }

        //Debug.Log(in_cookie +" : " + in_info);
    }


    /*
    IEnumerator pausePlayerController(float time)
    {
        PlayerController.allowControl = false;
        yield return new WaitForSeconds(time);
        PlayerController.allowControl = true;
    }
    */
}
