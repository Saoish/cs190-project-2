﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ghost : MonoBehaviour {
    public GameObject VanishVFX;
    public Rigidbody2D mybody;

    public static int dieGhosts = 0;

    public float Speed;
    // Use this for initialization
    void Start () {
        dieGhosts = 0;
        SoundManager.Post(SoundManager.GhostMoving, gameObject);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector2 Direction = (Vector2)PlayerController.Position - mybody.position;
        Direction.Normalize();

        
        mybody.MovePosition(mybody.position + Direction * Speed * Time.fixedDeltaTime);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            PlayerController.hurt();            
            vanish();
        }
    }

    public void vanish()
    {
        

        if (Panic_detector.ghosts.Contains(transform))
        {
            Panic_detector.ghosts.Remove(transform);

            //Debug.Log("ghost vanish! ");
        }
        Instantiate(VanishVFX, new Vector3(transform.position.x,transform.position.y,VanishVFX.transform.position.z), Quaternion.identity);
        SoundManager.Post(SoundManager.GhostVanish, gameObject);

        dieGhosts++;
        Objective.SetText("Kill Ghosts (with flashlight): " + dieGhosts.ToString() + "/" + generator.instance.ghosts.Count);

        if (dieGhosts == generator.instance.ghosts.Count)
        {
            ObjectiveManager.Next();
            ObjectivePointer.instance.gameObject.SetActive(true);
        }
        
        Destroy(gameObject);

    }
}
