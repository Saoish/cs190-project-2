﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Adding this allows us to access members of the UI namespace including Text.
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {        

	private float DefaultSpeed = 10f;             //Floating point variable to store the player's movement speed
    private float SpeedFacotr = 1.5f;    

	private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.
	private int count;              //Integer to store the number of pickups collected so far.
            
    private float WalkTimer = 0.0f;
    private float Speed;
    private float Stamina = 100f;
    private float RunThreshold = 30f;
    private bool Running = false;
    private float FootStepInterval {
        get { return (DefaultSpeed / Speed) / SpeedFacotr; }        
    }
    
    public bool IsWalking {get {return MoveVector != Vector2.zero;}}
    private bool AllowRun = true;
    private float RunTimer = 0f;
    

    public static bool allowControl = false;

    public static Interactable_Events IE = null;        //static for player IE

    public static Vector2 Position {
        get { return instance.rb2d.position; }
        set { instance.rb2d.position = value; }
    }

    public static PlayerController instance;

    private Vector2 MoveVector = Vector2.zero;

    private Vector2 LastPosition;

    public enum walkingState {concrete,bed,water,wood};
    public walkingState playerWalkingState = walkingState.concrete;

    private int defaultHealth = 3;
    public int myhealth = 3;
    public Image healthMask;
    public Image staminaMask;

    void Awake()
    {
        instance = this;
        defaultHealth = myhealth;
    }

	// Use this for initialization
	void Start()
	{        
        rb2d = GetComponent<Rigidbody2D> ();
        Speed = DefaultSpeed;
	}     

    void Update(){
        if (allowControl) {
            if (Input.GetKeyDown(KeyCode.E)) {
                if (IE != null) {
                    IE.interact();
                }
            }              
        }        

        if (Input.GetKey(KeyCode.LeftShift) && Stamina > 0 && IsWalking && AllowRun) {            
            Speed = DefaultSpeed + 10;
            if (Stamina > 0) {
                Running = true;
                float DeplatingRate = 20 * Time.deltaTime;
                if (Stamina - DeplatingRate < 0) {                    
                    Stamina = 0;
                    AllowRun = false;
                }
                else
                    Stamina -= DeplatingRate;
                if (Stamina < RunThreshold) {
                    staminaMask.color = Color.Lerp(staminaMask.color, Color.red, 1f * Time.deltaTime);                    
                }
                if (RunTimer >= 0.7f) {
                    if (!Panic_detector.isPanicking) {
                        SoundManager.Post(SoundManager.RapidBreath, gameObject);
                    }
                    RunTimer = 0f;                    
                }
                else {
                    RunTimer += Time.deltaTime;
                }
            }
        }
        else if ((Input.GetKeyUp(KeyCode.LeftShift) || !IsWalking) && Stamina < RunThreshold && AllowRun){
            AllowRun = false;
        }            
        else {
            Running = false;
            Speed = DefaultSpeed;
            if (Stamina < 100) {
                float GrowingRate = 5 * Time.deltaTime;
                if (Stamina + GrowingRate > 100) {
                    Stamina = 100;
                }
                else {
                    Stamina += GrowingRate;
                }
                if (Stamina > RunThreshold) {                    
                    staminaMask.color = Color.Lerp(staminaMask.color, Color.yellow, 1f * Time.deltaTime);
                    AllowRun = true;
                }
            }            
        }
        staminaMask.fillAmount = Stamina / 100;
    }
    
	//FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
	void FixedUpdate(){

        float moveHorizontal = Input.GetAxisRaw ("Horizontal");        		
		float moveVertical = Input.GetAxisRaw ("Vertical");


        if (allowControl)
        {
            MoveVector = new Vector2(moveHorizontal, moveVertical);
            MoveVector.Normalize();
            rb2d.MovePosition(rb2d.position + MoveVector * Speed * Time.fixedDeltaTime);
            if (IsWalking)
            {
                if (WalkTimer == 0)
                {
                    if (Position != LastPosition) {

                        switch(playerWalkingState)
                        {
                            case walkingState.concrete:
                                SoundManager.Post(SoundManager.PlayerWalk, gameObject);     //player walking sound
                                break;
                            case walkingState.bed:
                                SoundManager.Post(SoundManager.PlayerWalkCarpet, gameObject);     //player walking sound
                                break;
                            case walkingState.water:
                                SoundManager.Post(SoundManager.PlayerWalkWater, gameObject);     //player walking sound
                                break;
                            case walkingState.wood:
                                SoundManager.Post(SoundManager.PlayerWalkWood, gameObject);     //player walking sound
                                break;
                            default:
                                break;
                        }
                        
                        WalkTimer += Time.fixedDeltaTime;
                    }                    
                }
                else if (WalkTimer >= FootStepInterval)
                {
                    WalkTimer = 0;
                }
                else
                {
                    WalkTimer += Time.fixedDeltaTime;
                }
                LastPosition = Position;
            }
            else
            {
                WalkTimer = 0;
            }


        }
        else
        {
            //objective failed, disable movement
            MoveVector = Vector2.zero;
            
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "concrete")
        {
            playerWalkingState = walkingState.concrete;
        }
        if (other.gameObject.tag == "bed")
        {
            playerWalkingState = walkingState.bed;
        }
        if (other.gameObject.tag == "water")
        {
            playerWalkingState = walkingState.water;
        }
        if (other.gameObject.tag == "wood")
        {
            playerWalkingState = walkingState.wood;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "interaction")
        {

            Interactable_Events toCompare = other.GetComponent<Interactable_Events>();

            if(!toCompare.interactable)
            {
                return;
            }

            if (IE == null)
            {
                IE = toCompare;
            }
            else
            {
                if (toCompare.priority > IE.priority)
                {
                    IE = toCompare;
                }
            }
            IE.indicate();
        }

        // beside interaction:
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "interaction")
        {

            Interactable_Events toCompare = other.GetComponent<Interactable_Events>();
            if (IE == toCompare)
            {
                if (IE.interactable)
                    Hint.reset();
                IE = null;
            }
        }

        if (other.gameObject.tag == "bed")
        {
            playerWalkingState = walkingState.concrete;
        }
        if (other.gameObject.tag == "water")
        {
            playerWalkingState = walkingState.concrete;
        }
        if (other.gameObject.tag == "wood")
        {
            playerWalkingState = walkingState.concrete;
        }

        
        // beside interaction:
    }

    public static void hurt()
    {
        instance.myhealth -= 1;
        SoundManager.Post(SoundManager.PlayerHurt, instance.gameObject);

        instance.healthMask.fillAmount = instance.myhealth / (float)instance.defaultHealth;

        if (instance.myhealth == 0)
        {
            //endgame:
            SoundManager.Post(SoundManager.PlayerDying, instance.gameObject);
            allowControl = false;
            Debug.Log("You are Dead!");               
            EndGameUI.PopUpLoss();
        }
    }

    IEnumerator startnighttime()
    {
        yield return new WaitForSeconds(0.1f);
        SoundManager.Post(SoundManager.NightTime, gameObject);
    }

    /*
    public void footstepStop(object in_cookie, AkCallbackType in_type, object in_info)
    {

        if (in_type == AkCallbackType.AK_EndOfEvent)
        {
            //Debug.Log("footstep stop!");

        }

    }


    
    //OnTriggerEnter2D is called whenever this object overlaps with a trigger collider.
    void OnTriggerEnter(Collider other) 
    {
        //Check the provided Collider2D parameter other to see if it is tagged "PickUp", if it is...


         * if (other.gameObject.CompareTag("PickUp"))

            //... then set the other object we just collided with to inactive.
            other.gameObject.SetActive(false);

        //Add one to the current value of our count variable.
        count = count + 1;

        //Update the currently displayed count by calling the SetCountText function.
        SetCountText ();

    }

    //This function updates the text displaying the number of objects we've collected and displays our victory message if we've collected all of them.
    void SetCountText()
    {
        //Set the text property of our our countText object to "Count: " followed by the number stored in our count variable.
        countText.text = "Count: " + count.ToString ();

        //Check if we've collected all 12 pickups. If we have...
        if (count >= 12)
            //... then set the text property of our winText object to "You win!"
            winText.text = "You win!";
    }
    */
}

