﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panic_detector : MonoBehaviour {


    public static List<Transform> ghosts = new List<Transform>();

    public static bool isPanicking = false;

    private float min_cap = 1f;

    private float PanicFactor = 10f;
    private float panick_timer = 0.0f;
    private float PanickInterval(float closest_distance)
    {
        //Debug.Log("closest distance:" + closest_distance);

        if (closest_distance / PanicFactor < min_cap)
        {
            return min_cap;
        }

        

        //Debug.Log("interval : " + (closest_distance / PanicFactor));


        return closest_distance / PanicFactor;

    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {

        if (ghosts.Count == 0)
        {
            isPanicking = false;
            return;
        }
        else
        {
            isPanicking = true;
        }

        if (isPanicking)
        {
            float closest_distance = 999f;
            foreach (var ghost in ghosts)
            {
                float distance = Vector2.Distance(PlayerController.Position, ghost.position);
                if (distance < closest_distance)
                {
                    closest_distance = distance;

                }
            }

            if (panick_timer == 0)
            {
                SoundManager.Post(SoundManager.Panic, gameObject);

                panick_timer += Time.fixedDeltaTime;
            }
            else if (panick_timer >= PanickInterval(closest_distance))
            {
                panick_timer = 0;
            }
            else
            {
                panick_timer += Time.fixedDeltaTime;
            }

        }                
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.tag == "ghost")
        {
           
            if(!ghosts.Contains(other.transform))
            {
                ghosts.Add(other.transform);
            }
            //Debug.Log("ghost enter!");
        }
    }

    void OnTriggerExist2D(Collider2D other)
    {
        if (other.tag == "ghost")
        {
            if (ghosts.Contains(other.transform))
            {
                ghosts.Remove(other.transform);
            }
            //Debug.Log("ghost exit!");
        }
    }
}
