﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectivePointer : MonoBehaviour {
    public GameObject Visual;
    public static ObjectivePointer instance;
    float AngleZ;

	// Use this for initialization
	void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
        if (PlayerController.allowControl) {
            Visual.SetActive(true);
            Vector2 Direction = ObjectiveManager.Current.Position - (Vector2)transform.position;
            Direction.Normalize();
            AngleZ = Mathf.Atan2(Direction.y, Direction.x) * Mathf.Rad2Deg;
            transform.localEulerAngles = new Vector3(0, 0, AngleZ);
        }
        else {
            Visual.SetActive(false);
        }
    }

    public static void disable_arrow()
    {
        instance.Visual.SetActive(false);
    }
}
