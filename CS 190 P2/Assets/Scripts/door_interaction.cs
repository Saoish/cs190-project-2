﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class door_interaction : Interactable_Events
{

    private Animator door_animator;
    private bool open_state = false; //false = closed

    protected override void Start()
    {
        base.Start();
        door_animator = transform.parent.GetComponentInChildren<Animator>();        
    }

    public override void indicate() {

        if (open_state == false)
        {
            Hint.show("'E' open");
        }
        else
        {
            Hint.show("'E' close");
        }
    }

    IEnumerator delay_reactive()
    {
        yield return new WaitForSeconds(1f);
        interactable = true;
    }
    public override void interact()
    {
        if (interactable)
        {
            if (open_state == false)
            {
                SoundManager.Post(SoundManager.DoorOpen, gameObject);
                door_animator.Play("door_open");
                open_state = true;
            }
            else
            {
                SoundManager.Post(SoundManager.DoorClose, gameObject);
                door_animator.Play("door_close");
                open_state = false;
            }
           
            PlayerController.IE = null;
            interactable = false;
            Hint.reset();
            //ObjectiveManager.Next();

            StartCoroutine(delay_reactive());

        }
    }
}
