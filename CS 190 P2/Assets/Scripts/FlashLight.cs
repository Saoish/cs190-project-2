﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour {

    float AngleZ;

    public static FlashLight instance;

	// Use this for initialization
	void Start () {
        instance = this;
        TurnOff();
	}

    public static void TurnOn() {
        instance.gameObject.SetActive(true);
        //SFX:
        SoundManager.Post(SoundManager.pickupFlashlight, instance.gameObject);
        
    }

    public static void TurnOff() {
        instance.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (PlayerController.allowControl) {
            Vector2 self_pos = Camera.main.WorldToViewportPoint(transform.position);
            Vector2 mouse_pos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            AngleZ = Mathf.Atan2(mouse_pos.y - self_pos.y, mouse_pos.x - self_pos.x) * Mathf.Rad2Deg;
            transform.localEulerAngles = new Vector3(0, 0, AngleZ);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "ghost")
        {
            other.GetComponent<ghost>().vanish();
        }
    }
}
