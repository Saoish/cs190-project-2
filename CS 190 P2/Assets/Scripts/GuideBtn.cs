﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideBtn : MonoBehaviour {
    public Animator GuideAnim;
    bool Expanded = false;

    public void Toggle() {
        if (Expanded) {
            GuideAnim.Play("slide_in");
            Expanded = false;
        }
        else {
            GuideAnim.Play("slide_out");
            Expanded = true;
        }
        SoundManager.Post(SoundManager.Swoosh, gameObject);
    }
}
