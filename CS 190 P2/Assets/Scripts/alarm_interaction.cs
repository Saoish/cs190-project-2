﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class alarm_interaction : Interactable_Events {

    public float Delay = 10f;

    IEnumerator StartDelay() {
        SoundManager.Post(SoundManager.SnoreActive, PlayerController.instance.gameObject);
        yield return new WaitForSeconds(Delay);
        //yield return new WaitForSeconds(0);
        SoundManager.Post(SoundManager.SnoreOff, PlayerController.instance.gameObject);
        SoundManager.Post(SoundManager.AlarmOn, gameObject);        
        Objective.SlideIn();
        PlayerController.allowControl = true;
    }

    protected override void Start() {
        base.Start();
        StartCoroutine(StartDelay());
    }

    public override void indicate() {
        Hint.show("'E' turn off.");
    }

    public override void interact() {
        if (interactable) {
                
            SoundManager.Post(SoundManager.AlarmOff, gameObject);//Stop SFX
            PlayerController.IE = null;
            interactable = false;
            ObjectiveManager.Next();
            Hint.reset();
        }
    }



}
