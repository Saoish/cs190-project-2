﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generator : MonoBehaviour {
    public float ShowerCompleteDelay;
    public float GeneratorDownDelay;
    public float DogDieDelay;
    public float ResumeControlDelay;
    public float HorrorDelay;    
    public float GhostsSummonDelay;

    public static generator instance;
    public Material temp;
    public SpriteRenderer myWetFloor;
    public dog_interaction dog_interaction;

    public GameObject myghostprefab;    
    public List<ghost> ghosts;    

    IEnumerator GeneratorDownProcess() {
        yield return new WaitForSeconds(ShowerCompleteDelay);
        ObjectiveManager.Next();

        yield return new WaitForSeconds(GeneratorDownDelay);        
        instance.GetComponent<Animator>().Play("generatordown");        
        SoundManager.Post(SoundManager.GeneratorDown, instance.gameObject);        
        instance.myWetFloor.material = instance.temp;        

        yield return new WaitForSeconds(DogDieDelay);
        dog_interaction.die();

        yield return new WaitForSeconds(ResumeControlDelay);        
        PlayerController.allowControl = true;

        yield return new WaitForSeconds(HorrorDelay);
        SoundManager.playHorrorSong();        

        yield return new WaitForSeconds(GhostsSummonDelay);
        foreach (var g in ghosts) {
            g.gameObject.SetActive(true);
        }        
    }

    void Awake () {
        instance = this;
	}
	
	public static void generatorDown()
    {
        instance.StartCoroutine(instance.GeneratorDownProcess());
    }
}
