﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveManager : MonoBehaviour {

    public List<Interactable_Events> Objectives;
    
    static int CurrentIndex;

    public static ObjectiveManager instance;   


    private void Awake() {
        instance = this;
        CurrentIndex = 0;
        Current.interactable = true;
    }

    private void Start() {        
    }

    public static Interactable_Events Current { get { return instance.Objectives[CurrentIndex]; } }

    public static void Next() {



        if (CurrentIndex != instance.Objectives.Count - 1) {
                        
            CurrentIndex++;
            Current.interactable = true;
            Objective.Set(Current);

            //SFX:
            SoundManager.Post(SoundManager.ObjectiveDone, instance.gameObject);

            if(Current is killingGhosts)
            {                
                ObjectivePointer.instance.gameObject.SetActive(false);                
            }
        }
        else {//Win Game Condiction'
            Objective.Set(Current);
            SoundManager.Post(SoundManager.ObjectiveDone, instance.gameObject);
        }
    }

}
