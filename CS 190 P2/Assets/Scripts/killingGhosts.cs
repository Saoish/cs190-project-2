﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killingGhosts : Interactable_Events
{
    
    protected override void Start()
    {
        base.Start();
        objective_description = "Kill Ghosts (with flashlight): 0/" + generator.instance.ghosts.Count;

    }

    public override void indicate()
    {
        throw new NotImplementedException();
    }

    public override void interact()
    {
        throw new NotImplementedException();
    }
}
