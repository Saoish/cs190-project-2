﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class Btn : Button {

    public override void OnPointerEnter(PointerEventData eventData) {
        base.OnPointerEnter(eventData);
        SoundManager.Post(SoundManager.ButtonHover, gameObject);
    }

    public override void OnSelect(BaseEventData eventData) {
        base.OnSelect(eventData);
        SoundManager.Post(SoundManager.ButtonHover, gameObject);
    }

    public override void OnSubmit(BaseEventData eventData) {
        base.OnSubmit(eventData);
        SoundManager.Post(SoundManager.ButtonClick, gameObject);
    }
}
