﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EndGameUI : MonoBehaviour {
    public static EndGameUI instance;
    public Text EndText;
    public Animator PopUpAnim;
    public GameObject Guide;
    public GameObject Mask;
    private void Start() {
        instance = this;
    }


    void AfterClickAndReload(object in_cookie, AkCallbackType in_type, object in_info) {
        if (in_type == AkCallbackType.AK_EndOfEvent) {
            SoundManager.Post(SoundManager.EndGame, gameObject);
            Application.LoadLevel(0);
        }
    }

    public void Restart() {
        GetComponent<CanvasGroup>().interactable = false;        
        Mask.SetActive(true);
        SoundManager.Post(SoundManager.ButtonClick, gameObject, AfterClickAndReload);
    }

    void AfterClickAndQuit(object in_cookie, AkCallbackType in_type, object in_info) {
        if (in_type == AkCallbackType.AK_EndOfEvent) {
            Application.Quit();
        }
    }

    public void Quit() {
        GetComponent<CanvasGroup>().interactable = false;
        Mask.SetActive(true);
        SoundManager.Post(SoundManager.ButtonClick, gameObject, AfterClickAndQuit);
    }

    public static void PopUpLoss() {
        instance.Guide.SetActive(false);
        instance.EndText.color = Color.red;
        instance.EndText.text = "Died";
        instance.PopUpAnim.Play("end_game_ui_pop");
    }

    public static void PopUpWin() {
        instance.Guide.SetActive(false);
        instance.EndText.color = Color.yellow;
        instance.EndText.text = "Escaped";
        instance.PopUpAnim.Play("end_game_ui_pop");
    }    
}
