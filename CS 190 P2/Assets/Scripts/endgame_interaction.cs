﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endgame_interaction : Interactable_Events
{

    public override void indicate()
    {        
    }

    public override void interact()
    {
        throw new NotImplementedException();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player" && interactable)
        {
            //start end game menu:
            Debug.Log("the end!");
            PlayerController.allowControl = false;
            ObjectiveManager.Next();
            EndGameUI.PopUpWin();
        }
    }
}
