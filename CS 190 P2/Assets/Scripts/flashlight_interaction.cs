﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flashlight_interaction : Interactable_Events
{
    public GameObject myflashlight;

    public override void indicate()
    {
        Hint.show("'E' Pick up flashlight");
    }

    public override void interact()
    {

        if (interactable)
        {

            SoundManager.Post(SoundManager.pickupFlashlight, gameObject);

            FlashLight.TurnOn();
            myflashlight.SetActive(false);

            Hint.reset();
            ObjectiveManager.Next();
            interactable = false;
        }

    }
}
