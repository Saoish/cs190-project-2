﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Objective : MonoBehaviour {
    public float Delay;
    public Text text;
    public Animator slide_in_anim;
    public Animator check_anim;
    public GameObject check;
    public static Objective instance;

    // Use this for initialization
    private void Awake() {
        instance = this;
    }

    public static void SlideIn() {        
        instance.slide_in_anim.Play("slide_in");
    }
    
	void Start () {
        text.text = ObjectiveManager.Current.objective_description;
    }
	
    public static void Set(Interactable_Events IE) {
        instance.StartCoroutine(instance.SetWithDelay(IE));
    }    

    IEnumerator SetWithDelay(Interactable_Events IE) {
        instance.check.SetActive(true);
        instance.check_anim.Play("check", 0, 0);
        yield return new WaitForSeconds(Delay);
        instance.text.text = "Objective: " + IE.objective_description;
        instance.check.SetActive(false);
    }
        

    public static void SetText(string mytext)
    {
        instance.text.text = mytext;
    }
}
