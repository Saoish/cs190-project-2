﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hint : MonoBehaviour {


    public static Text myState;

	// Use this for initialization
	void Start () {
        myState = GetComponent<Text>();
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void show(string text)
    {
        myState.text = text;
    }
    public static void reset()
    {
        myState.text = "";
    }
}
