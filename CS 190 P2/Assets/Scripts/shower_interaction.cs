﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shower_interaction : Interactable_Events
{
    public SpriteRenderer myWaterSprite;

    public float delayGeneratordown = 5f;

    [HideInInspector]
    public int waterAlphaCap = 150;

    protected override void Start() {
        base.Start();
    }

    IEnumerator make_it_wet()
    {
        myWaterSprite.gameObject.SetActive(true);
        while(myWaterSprite.color.a < 0.7)
        {
            Color temp = myWaterSprite.color;
            temp.a += 0.1f * Time.deltaTime;
            myWaterSprite.color = temp;
            // Debug.Log(temp.a);

            yield return new WaitForEndOfFrame();
        }
        
    }

    public override void indicate() {
        Hint.show("'E' shower");
    }

    public override void interact()
    {

        if (interactable)
        {

            StartCoroutine(make_it_wet());            
            generator.generatorDown();
            Debug.Log("Freeze! taking a shower now!");
            PlayerController.allowControl = false;
            //SoundManager.Post(SoundManager.ShowerActive, gameObject, afterShower);
            SoundManager.Post(SoundManager.ShowerActive, gameObject);
            //animation taking shower:

            PlayerController.IE = null;
            interactable = false;
            Hint.reset();
            //ObjectiveManager.Next();
        }            
    }

    public void afterShower(object in_cookie, AkCallbackType in_type, object in_info)
    {

        if (in_type == AkCallbackType.AK_EndOfEvent)
        {
            Debug.Log("stop taking shower!!!");
            PlayerController.allowControl = true;
            //AkEventCallbackInfo info = (AkEventCallbackInfo)in_info; //Then do stuff.

        }
     
    }
}
